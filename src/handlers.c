/**
 * @file        handlers.c
 * @author      Adam Malinowski <upanie@interia.pl>
 * @brief       handlers source file
 */

#include <FreeRTOS.h>
#include <task.h>

void sv_call_handler(void);
void pend_sv_handler(void);
void sys_tick_handler(void);

void vPortSVCHandler(void);
void xPortSysTickHandler(void);
void xPortPendSVHandler(void);

void vApplicationMallocFailedHook(void);
void vApplicationIdleHook(void);
void vApplicationTickHook(void);
void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName);


void vApplicationMallocFailedHook( void ) {
	taskDISABLE_INTERRUPTS();
    for ( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void ) {
//	xprintf("IDLE\n");
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName) {
    (void) pcTaskName;
    (void) pxTask;
    taskDISABLE_INTERRUPTS();
    for ( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void ) {
}

void sv_call_handler(void) {
      vPortSVCHandler();
}
 void pend_sv_handler(void) {
      xPortPendSVHandler();
}
void sys_tick_handler(void) {
      xPortSysTickHandler();
}
