#include <stdio.h>

#include <libopencm3/stm32/rcc.h>

#include <FreeRTOS.h>
#include <task.h>

#include <uart.h>
#include <tasks.h>


static void clock_setup(void) {
    rcc_clock_setup_in_hsi_out_48mhz();
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_USART1);
}

int main(void) {
    clock_setup();
    uart_setup();

    create_tasks();

    vTaskStartScheduler();

    return 0;
}

