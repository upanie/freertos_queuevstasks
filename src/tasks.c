/**
 * @file        tasks.c
 * @author      Adam Malinowski <upanie@interia.pl>
 * @brief       tasks source file
 */

#include <stdio.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include <uart.h>
#include <tasks.h>

void task1(void *pvData);
void task2(void *pvData);

uint8_t data;
QueueSetMemberHandle_t activated_queue;

void do_something(void) {
	volatile int i;
	for (i=0; i < 1000; i++) {

	}
}

void task1(void *pvData) {
    (void)pvData;
    vTaskDelay(100);

    while (1) {
        while (xQueueReceive(usart_incoming_queue, &data, portMAX_DELAY) == pdTRUE) {
        	do_something();
        }
    }
}

void task2(void *pvData) {
    (void)pvData;
//    int it = 0;

    while (1) {
    	vTaskDelay(5000);
    	do_something();
//    	xQueueSendToBack(usart_incoming_queue, &data, portMAX_DELAY);
    }
}

xTaskHandle th_1, th_2;
void create_tasks(void) {
    xTaskCreate(task1, (const char *) "TSK1", 128, NULL, tskIDLE_PRIORITY+1, &th_1);
    xTaskCreate(task2, (const char *) "TSK2", 128, NULL, tskIDLE_PRIORITY+1, &th_2);
}
