/**
 * @file        uart.c
 * @author      Adam Malinowski <upanie@interia.pl>
 * @brief       uart source file
 */

#include <stddef.h>

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/rcc.h>

#include <uart.h>

#define USART_INCOMING_QUEUE_LENGTH 32

xQueueHandle usart_incoming_queue;

void usart1_isr(void) {
	long xHigherPriorityTaskWoken = pdFALSE;
    /* Check if we were called because of RXNE. */
    if (((USART_CR1(USART1) & USART_CR1_RXNEIE) != 0) &&
        ((USART_ISR(USART1) & USART_ISR_RXNE) != 0)) {

        /* Retrieve data from the peripheral. */
        uint8_t data = usart_recv(USART1);
        xQueueSendToBackFromISR(usart_incoming_queue, &data, &xHigherPriorityTaskWoken);
    }
    portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}

void uart_setup(void) {

	usart_incoming_queue = xQueueCreate(USART_INCOMING_QUEUE_LENGTH, sizeof(char));

    nvic_enable_irq(NVIC_USART1_IRQ);

    /* Setup GPIO pins for USART2 transmit. */
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO9 | GPIO10);
    /* Setup USART1 TX pin as alternate function. */
    gpio_set_af(GPIOA, GPIO_AF1, GPIO9 | GPIO10);

    /* Setup UART parameters. */
    usart_set_baudrate(USART1, 115200);
    usart_set_databits(USART1, 8);
    usart_set_stopbits(USART1, USART_CR2_STOPBITS_1);
    usart_set_parity(USART1, USART_PARITY_NONE);
    usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
    usart_set_mode(USART1, USART_MODE_TX_RX);

    /* Enable USART1 Receive interrupt. */
    usart_enable_rx_interrupt(USART1);

    /* Finally enable the USART. */
    usart_enable(USART1);
}

void uart_putc(char data) {
    usart_send_blocking(USART1, data);
}
