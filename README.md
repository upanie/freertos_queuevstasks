# FreeRTOS_queueVStasks

This simple example reproduces bug found in either FreeRTOS or ar-none-eabi-gcc compiler.
Or maybe I do something wrong here... no way ;)

There are two simple tasks. One is sending some text to UART and the second one
waits for characters from a queue. If a character gets into the queue, the code reads
it from the queue and sends it to the UART.
Characters are put int queue in UART RX interrupt but it can be done also in the other task.
There is one commented line which can do that.

**The problem**

The whole code works perfectly when it is built with:
*arm-none-eabi-gcc (GNU Tools for ARM Embedded Processors) 4.8.4 20140526 (release) [ARM/embedded-4_8-branch revision 211358]*
When built with:
*arm-none-eabi-gcc (GNU Tools for Arm Embedded Processors 8-2018-q4-major) 8.2.1 20181213 (release) [gcc-8-branch revision 267074]*
the code fails when a character is in the queue and the thread tries to read it.
arm-none-eabi-gcc (GNU Tools for ARM Embedded Processors) 5.3.1 20160307 (release) [ARM/embedded-5-branch revision 234589]
is even worse. It hangs even without any character in the queue.

I did some investigation and it looks like something overwrites part of stack of task1.
The code goes to blocking_handler which is called by hard float handler.
xQueueReceive has broken address of the queue structure which is a bit weird because the address is held in register R0.
I tried to find what is going on but I simply failed. Took me a lot of time and all I can say is that
the compiler does something bad ;)

**CLONE**

git clone https://upanie@bitbucket.org/upanie/freertos_queuevstasks.git

**BUILD**

cd freertos_queuevstasks
git submodule init
git submodule update
cd libopencm3
make
cd ../src
make


**UPDATE**
- Now the code does not use xprintf as suggested by other that it might coase stack overflow. The only way to see what's happening is to debug it using debuger.
