/**
 * @file        uart.h
 * @author      Adam Malinowski <upanie@interia.pl>
 * @brief       uart include file
 */

#ifndef INCLUDE_UART_H_
#define INCLUDE_UART_H_

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>

#include <FreeRTOS.h>
#include <queue.h>

extern xQueueHandle usart_incoming_queue;

void uart_setup(void);
void uart_send(char *data);
void uart_printf(const char *fmt, ...);
void uart_putc(char data);

void uart_set_voltage(int mvolts);
int uart_get_voltage(void);

#endif /* INCLUDE_UART_H_ */
