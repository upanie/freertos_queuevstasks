/**
 * @file        tasks.h
 * @author      Adam Malinowski <upanie@interia.pl>
 * @brief       tasks include file
 */

#ifndef INCLUDE_TASKS_H_
#define INCLUDE_TASKS_H_

#include <stdbool.h>

void create_tasks(void);

#endif /* INCLUDE_TASKS_H_ */
